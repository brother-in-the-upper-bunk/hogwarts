
import copy
a=[1,2,3,['a','b']]
#赋值
b=a
#浅拷贝
c=copy.copy(a)
#深拷贝
d=copy.deepcopy(a)
#修改a
a.append(4)
#修改a中嵌套的子对象
a[3].append('h')
#打印查看修改之后变量的值
print("a=",a)#a= [1, 2, 3, ['a', 'b', 'h'], 4]
print("b=",b)#b= [1, 2, 3, ['a', 'b', 'h'], 4]
print("c=",c)#c= [1, 2, 3, ['a', 'b', 'h']]
print("d=",d)#d= [1, 2, 3, ['a', 'b']]


