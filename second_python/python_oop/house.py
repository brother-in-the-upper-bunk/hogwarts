#定义类(画出图纸)
class House:
    #静态属性：类变量，在类中，在方法之外
    door=""
    floor=""

    #私有属性
    __key=True


    #构造函数
    def __init__(self,door,floor):
        print("构造函数")
        #在构造函数中定义实例变量
        self.kichen="cook"
        self.door=door
        self.floor=floor
    #动态方法
    def sleep(self):
        #局部变量：类中，方法中，不以self开头
        bed="席梦思"
        #实例变量：类中，方法中，以self.开头，作用域为类中所有的方法
        self.table="桌子上可以放东西"
        print(f"在房子里面可以躺在{bed}睡觉")

    def cook(self):
        #在其他方法中调用实例变量
        #print(self.table)
        print(self.kichen)
        print("在房子里面可以做饭吃")

    @classmethod
    def class_method(cls):
        print("类方法")


    def open_door(self):
        if self.__key==True:
            print("开门啦~")
        else:
            print("没钥匙，开不了门")

    def get_key(self):
        self.__open_save()
    #定义私有方法
    def __open_save(self):
        print("打开保险柜")



#实例化：变量（实例）=类名（）
north_house=House("white","black")
china_house=House(floor="red",door="yellow")
north_house.open_door()
north_house.get_key()
House.class_method()
# print(north_house.door)
# print(china_house.door)
# #定义北欧风房子的属性
# north_house.door="white"
# north_house.floor="black"
# #定义中式房子的属性
# china_house.door="yello"
# china_house.floor="red"

#north_house.sleep()
north_house.cook()
# print(north_house.door)
# print(china_house.door)
# #调用类的属性
# door=House.door
# print(door)
# #修改类的属性
# House.door="blue"
# print(House.door)
# print(north_house.door)
# print(china_house.door)



